import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:set_app/di/di.dart';
import 'package:set_app/presentation/auth/auth_block.dart';
import 'package:set_app/presentation/auth/auth_event.dart';
import 'package:set_app/presentation/auth/auth_state.dart';
import 'auth_password_page.dart';
import 'auth_phone_page.dart';

class AuthPage extends StatefulWidget {
  @override
  _AuthPageState createState() => _AuthPageState();
}

class _AuthPageState extends State<AuthPage> {
  AuthBloc _authBloc;
  AuthPhonePage authPhonePage;
  AuthPasswordPage authPasswordPage;
  bool showPhonePage = true;
  bool showLoading = false;
  String phone = "";

  @override
  void initState() {
    _authBloc = AuthBloc(InitialState(), getIt());
    authPhonePage = AuthPhonePage(phone);
    authPasswordPage = AuthPasswordPage();
    print(_authBloc.toString());
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Visibility(
        child: BlocBuilder<AuthBloc, AuthState>(
      builder: blocBuilder,
      cubit: _authBloc,
    ));
  }

  Widget blocBuilder(BuildContext context, AuthState state) {
    if (state is ShowError)
      ScaffoldMessenger.of(context)
          .showSnackBar(SnackBar(content: Text(state.error)));
    else if (state is Loading) {
      showLoading = state.show;
      print("showLoading:" + showLoading.toString());
    } else if (state is PhoneInit) {
      showPhonePage = true;
    } else if (state is PasswordInit) {
      showPhonePage = false;
    }
    return Scaffold(
        backgroundColor: Colors.white,
        body: Stack(children: [
          SingleChildScrollView(
            padding: EdgeInsets.all(16),
            child: Column(
              children: <Widget>[
                Padding(
                  padding: const EdgeInsets.only(top: 60, bottom: 20),
                  child: Center(
                    child: Container(
                        width: 200,
                        height: 150,
                        child: Image.asset('assets/icons/computer.png')),
                  ),
                ),
                showPhonePage ? authPhonePage : authPasswordPage
              ],
            ),
          ),
          if (showLoading)
            Container(
                alignment: Alignment.bottomCenter,
                child: CircularProgressIndicator())
          else if (showPhonePage)
            Container(
                alignment: Alignment.bottomCenter,
                child: SizedBox(
                  width: double.infinity,
                  child: FlatButton(
                    height: 50,
                    color: Colors.blueAccent,
                    // onPressed: () => widget.onSendClick(phoneTxtController.text),
                    onPressed: () {
                      _authBloc.add(PhoneClicked(authPhonePage.text));
                    },
                    child: Text(
                      'Готово',
                      style: TextStyle(color: Colors.white, fontSize: 15),
                    ),
                  ),
                ))
        ]));
  }
}
