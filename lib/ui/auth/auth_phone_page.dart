import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:mask_text_input_formatter/mask_text_input_formatter.dart';

class AuthPhonePage extends StatefulWidget {
  String text;

  AuthPhonePage(this.text);

  @override
  _AuthPhonePageState createState() => _AuthPhonePageState();
}

class _AuthPhonePageState extends State<AuthPhonePage> {
  final phoneTxtController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Container(
          padding: EdgeInsets.only(top: 70),
          alignment: Alignment.centerLeft,
          child: Text(
            "Авторизация",
            textAlign: TextAlign.left,
            style: TextStyle(
                fontWeight: FontWeight.bold, fontSize: 18, color: Colors.black),
          ),
        ),
        Container(
          padding: EdgeInsets.only(top: 24),
          alignment: Alignment.centerLeft,
          child: Text(
            "Укажите номер вашего телефона",
            textAlign: TextAlign.left,
          ),
        ),
        Stack(alignment: Alignment.centerLeft, children: [
          TextField(
            autofocus: true,
            keyboardType: TextInputType.phone,
            maxLength: 15,
            onChanged: (text) {
              widget.text = text;
            },
            cursorColor: Colors.blueAccent,
            controller: phoneTxtController,
            inputFormatters: [
              new MaskTextInputFormatter(
                  mask: '00 000 00 00', filter: {"0": RegExp(r'[0-9]')})
            ],
            decoration: InputDecoration(
                prefix: Text("+998"),
                counterText: "",
                prefixStyle: TextStyle(
                    fontSize: 30,
                    fontWeight: FontWeight.bold,
                    color: Colors.black),
                border: InputBorder.none,
                hintStyle:
                TextStyle(color: Colors.grey, fontWeight: FontWeight.bold),
                fillColor: Colors.white),
            textAlign: TextAlign.left,
            style: TextStyle(
                fontSize: 30, fontWeight: FontWeight.bold, color: Colors.black),
          ),
        ])
      ],
    );
  }
}
