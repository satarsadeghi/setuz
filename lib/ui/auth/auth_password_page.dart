import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:mask_text_input_formatter/mask_text_input_formatter.dart';

class AuthPasswordPage extends StatefulWidget {
  @override
  _AuthPasswordState createState() => _AuthPasswordState();
}

class _AuthPasswordState extends State<StatefulWidget> {
  @override
  Widget build(BuildContext context) {
    return  Column(
          children: <Widget>[
            Container(
              padding: EdgeInsets.only(top: 70),
              alignment: Alignment.centerLeft,
              child: Text(
                "Подтвердите номер",
                textAlign: TextAlign.left,
                style: TextStyle(
                    fontWeight: FontWeight.bold,
                    fontSize: 18,
                    color: Colors.black),
              ),
            ),
            Container(
              padding: EdgeInsets.only(top: 24),
              alignment: Alignment.centerLeft,
              child: Text(
                "Введите код из СМС",
                textAlign: TextAlign.left,
              ),
            ),
            Stack(alignment: Alignment.centerLeft, children: [
              TextField(
                autofocus: true,
                keyboardType: TextInputType.phone,
                maxLength: 4,
                cursorColor: Colors.blueAccent,
                inputFormatters: [
                  new MaskTextInputFormatter(
                      mask: '****', filter: {"0": RegExp(r'[0-9]')})
                ],
                decoration: InputDecoration(
                    counterText: "",
                    prefixStyle: TextStyle(
                        fontSize: 30,
                        fontWeight: FontWeight.bold,
                        color: Colors.black),
                    border: InputBorder.none,
                    hintStyle: TextStyle(
                        color: Colors.grey, fontWeight: FontWeight.bold),
                    fillColor: Colors.white),
                textAlign: TextAlign.left,
                style: TextStyle(
                    fontSize: 30,
                    fontWeight: FontWeight.bold,
                    color: Colors.black),
              ),
            ]),
          ],
        );
  }
}
