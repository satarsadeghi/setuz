import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:set_app/di/di.dart';
import 'package:set_app/ui/auth/auth_page.dart';
import 'package:get_it/get_it.dart';

void main() {
  configureInjection();
  runApp(MaterialApp(
    title: 'Set',
    home: App(),
  ));
}

class App extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'SetUz',
      debugShowCheckedModeBanner: false,
      home: AuthPage(),
    );
  }
}
