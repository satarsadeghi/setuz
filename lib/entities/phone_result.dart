class PhoneResult {
  final String success;

  PhoneResult(this.success);

  PhoneResult.fromJson(Map<String, dynamic> json) : success = json["success"];
}
