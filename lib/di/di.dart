import 'package:dio/dio.dart';
import 'package:get_it/get_it.dart';
import 'package:set_app/model/interactor/auth_interactor.dart';
import 'package:set_app/model/repository/auth/auth_repository.dart';
import 'package:set_app/model/server/api.dart';
import 'package:set_app/model/server/api_impl.dart';
import 'package:set_app/model/system/data/auth_holder.dart';
import 'package:set_app/model/system/data/auth_prefs.dart';

final getIt = GetIt.instance;

void configureInjection() {
  initProviders();
  initRepositories();
}

void initRepositories() {
  getIt.registerLazySingleton<Api>(() => ApiImpl(getIt()));
  getIt.registerLazySingleton<AuthRepository>(() => AuthRepository(getIt()));
  getIt.registerLazySingleton<AuthInteractor>(() => AuthInteractor(getIt(), getIt()));
}

void initProviders() {
  getIt.registerSingleton<AuthHolder>(AuthPrefs());
  dioProvider(getIt());
}

void dioProvider(AuthHolder authHolder) {
  BaseOptions options = BaseOptions(
      connectTimeout: Duration(seconds: 20).inMilliseconds,
      sendTimeout: Duration(seconds: 20).inMilliseconds,
      baseUrl: 'http://devapi.set.uz/');
  Dio dio = Dio(options);
  dio.interceptors.add(LogInterceptor(
    requestBody: true,
    responseBody: true,
  ));

  authHolder
      .getToken()
      .then((value) => {
            if (value.isNotEmpty)
                dio.options.headers
                    .putIfAbsent('Authorization', () => 'Token $value')
          })
      .whenComplete(() => getIt.registerFactory<Dio>(() => dio));
}
