import 'package:set_app/model/system/data/auth_holder.dart';
import 'package:shared_preferences/shared_preferences.dart';

class AuthPrefs extends AuthHolder {
  static const TOKEN = 'token';

  Future<SharedPreferences> prefs() => SharedPreferences.getInstance();

  @override
  Future<String> getToken() async {
    return (await prefs()).getString(TOKEN);
  }

  @override
  void setToken(String token) async {
    (await prefs()).setString(TOKEN, token);
  }
}
