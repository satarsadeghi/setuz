abstract class AuthHolder {
  void setToken(String token);

  Future<String> getToken();
}


