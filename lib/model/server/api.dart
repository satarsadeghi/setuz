import 'package:set_app/entities/phone_result.dart';

abstract class Api {
  Future<PhoneResult> signUpPhone(String request);
}
