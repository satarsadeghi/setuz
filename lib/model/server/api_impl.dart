import 'dart:convert';
import 'package:dio/dio.dart';
import 'package:set_app/entities/phone_result.dart';
import 'package:set_app/model/server/api.dart';


class ApiImpl extends Api {
  final Dio _dio;

  ApiImpl(this._dio);

  @override
  Future<PhoneResult> signUpPhone(String phone) async {
    Response response = await _dio.post("/api/v1/user/signup_send_code/",
        data: jsonEncode({"phone_number": "$phone"}));
    return PhoneResult.fromJson(response.data);
  }
}
