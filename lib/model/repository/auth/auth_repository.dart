import 'package:dartz/dartz.dart';
import 'package:set_app/entities/phone_result.dart';
import 'package:set_app/model/server/api.dart';

class AuthRepository {
  Api _api;

  AuthRepository(this._api);

  Future<Either< Exception,PhoneResult>> signUpPhone(String phone) async {
    try {
      var result = await _api.signUpPhone(phone);
      return Right(result);
    } on Exception catch (e) {
      return Left(e);
    }
  }
}
