import 'package:dartz/dartz.dart';
import 'package:set_app/entities/phone_result.dart';
import 'package:set_app/model/repository/auth/auth_repository.dart';
import 'package:set_app/model/system/data/auth_holder.dart';

class AuthInteractor {
   AuthRepository _authRepository;
   AuthHolder _authHolder;

  AuthInteractor(this._authRepository, this._authHolder);

  Future<Either<Exception, PhoneResult>> signUpPhone(String phone) =>
      _authRepository.signUpPhone(phone);



}
