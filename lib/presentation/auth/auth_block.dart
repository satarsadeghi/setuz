import 'package:bloc/bloc.dart';
import 'package:set_app/model/interactor/auth_interactor.dart';

import 'auth_event.dart';
import 'auth_state.dart';

class AuthBloc extends Bloc<AuthEvent, AuthState> {
  AuthBloc(AuthState initialState, this._authInteractor) : super(initialState);

  AuthInteractor _authInteractor;

  @override
  Stream<AuthState> mapEventToState(AuthEvent event) async* {
    print(event.toString());
    if (event is PhoneClicked) {
      var phone = event.phoneNumber;
      print(phone);
      if (phone.length == 12){
        yield Loading(true);
        yield* _sendPassword("+998" + phone.replaceAll(" ", ""));
      }
    }
  }

  Stream<AuthState> _sendPassword(String phone) async* {
    AuthState state = (await _authInteractor.signUpPhone(phone)).fold(
        (error) => ShowError(error.toString()), (value) => PasswordInit());
    yield state;
    print(state.toString());
    yield Loading(false);
    print("Loading false"  );
  }
}
