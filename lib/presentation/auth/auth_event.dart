import 'package:equatable/equatable.dart';

class AuthEvent extends Equatable {
  @override
  List<Object> get props => [];
}

class PhoneChange extends AuthEvent {
  final String phone;

  PhoneChange(this.phone);

  @override
  List<Object> get props => [phone];
}

class PhoneClicked extends AuthEvent {
  final String phoneNumber;

  PhoneClicked(this.phoneNumber);

  @override
  List<Object> get props => [phoneNumber];
}

class PasswordSendClicked extends AuthEvent {
  final String password;

  PasswordSendClicked(this.password);

  @override
  List<Object> get props => [password];
}

class ResendPasswordClicked extends AuthEvent {
  final String phoneNumber;

  ResendPasswordClicked(this.phoneNumber);

  @override
  List<Object> get props => [phoneNumber];
}

class OnBackClicked extends AuthEvent {}
