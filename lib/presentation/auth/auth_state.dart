import 'package:equatable/equatable.dart';


abstract class AuthState extends Equatable {
  @override
  List<Object> get props => [];
}
class InitialState extends AuthState { }

class Loading extends AuthState {
  final bool show;

  Loading(this.show);

  @override
  List<Object> get props => [show];
}

class PhoneInit extends AuthState {}

class PasswordInit extends AuthState {}

class ShowError extends AuthState {
  final String error;

  ShowError(this.error);

  @override
  List<Object> get props => [error];
}

class AuthSuccess extends AuthState {}
